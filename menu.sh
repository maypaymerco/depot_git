#!/usr/bin/env bash
#Auteur : Merco May-pay
#date : 15/03/2021 

clear 
function menu {
while true 
do 
 echo "Je ne peux que vous être utile si vous êtes dehors. Etes-vous dehors? (oui/non)"
 read rep 
 test $rep = "oui"
 test $rep = "non" && break 

	while (true) 
	do  
		echo "Que voulez-vous faire? : " 
		echo "1) configurer le joystick" 
		echo "2) lancer l'enregistrement de trajectoire"
		echo "3) lancer le suivi de trajectoire" 
                echo "4) lancer l'arrêt d'urgence en manuel" 
		echo "5) lancer l'essai d'arrêt à la sortie de la zone" 
                echo "6) éteindre le pc" 
		echo "Q) Quiter dans l'application ARPA" 
		echo ""
		read -p "faites votre choix : " choix   
		echo ""
		case $choix in

		 "1") 
		   sudo modprobe xpad 
                        ;;
		 "2")
		   romea_launch live src/romea_demos/demos/arpa/wgs84_path_recorder.demo
			;;
		 "3") 
		   romea_launch live src/romea_demos/demos/arpa/wgs84_path_following.demo
                        ;;
		 "4") 
                   romea_launch live src/romea_demos/demos/arpa/emergency_stop.demo
			;; 
	         "5")
                   romea_launch live src/romea_demos/demos/arpa/arpa.demo #fait appel du demo
                        ;;
		 "6")
		   sudo shutdown -h now
			;;		
                 "q" | "Q"| "quiter" | "quit" )
		 exit 0
                        ;;
                 *)
                 echo "cette fonction n'est pas encore utiliser" 
esac 
done 
done 
}


echo "Bievennue sur le console ARPA !" 
echo ""

menu
exit 0




